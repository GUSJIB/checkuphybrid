import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { ProfilePage } from '../profile/profile';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  signIn() {
    this.presentLoading(() => {
      // this.navCtrl.push(ProfilePage);
      this.navCtrl.setRoot(ProfilePage, {}, {animate: true, direction: "forward"});
    });
  }

  forgotPassword() {
    console.log('Forgot Password Clicked.');
  }

  presentLoading(callback) {
    let loader = this.loadingCtrl.create({
      content: "Authenticating...",
      duration: 1500
    });
    loader.present();
    loader.onDidDismiss(callback);
  }

}
