import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { TermsOfServicePage } from '../terms-of-service/terms-of-service';

@Component({
  selector: 'page-get-password',
  templateUrl: 'get-password.html',
})
export class GetPasswordPage {

  email: String;
  hn: String;
  agree: false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GetPassword');
  }

  getPassword() {
    console.log('Submit form get password');
  }

  termsPersent() {
    this.modalCtrl.create(TermsOfServicePage).present();
  }

  agreeChange() {
    if(this.agree) {
      this.termsPersent();
    }
  }

}
