import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PhysicalExaminationPage } from '../physical-examination/physical-examination'; 


@Component({
  selector: 'page-examination-list',
  templateUrl: 'examination-list.html',
})
export class ExaminationListPage {

  constructor(public navCtrl: NavController, public NavParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad checkuplist');
  }
 goToGetresult(orderitem) {
     this.navCtrl.push(PhysicalExaminationPage , { orderitem: orderitem});
    
  }



}
