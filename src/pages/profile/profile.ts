import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController } from 'ionic-angular';

import { PatientsHistoryPage } from '../patients-history/patients-history';
import { MyPackagePage } from '../my-package/my-package';
import { SettingPage } from '../setting/setting';
import { AppointmentPage } from '../appointment/appointment';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  tab: any;

  constructor(public navCtrl: NavController, public params: NavParams, public actionSheetCtrl: ActionSheetController) {
    this.tab = 'detail';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Profile', this.params.get('id'));
  }

  goToHistory() {
    this.navCtrl.push(PatientsHistoryPage);
  }

  goToPackage() {
    this.navCtrl.push(MyPackagePage);
  }

  goToSetting() {
    this.navCtrl.push(SettingPage);
  }

  goToAppointment() {
    this.navCtrl.push(AppointmentPage);
  }

  presentActionSheet(showBtnResult) {
   let actionSheet = this.actionSheetCtrl.create({
     title: 'Action',
     buttons: [
       {
         text: 'Direction to Here',
         handler: () => {
           console.log('Direction clicked');
         }
       },
       {
         text: 'Cancel',
         role: 'cancel',
         handler: () => {
           console.log('Cancel clicked');
         }
       }
     ]
   });

   if(showBtnResult != false){
      actionSheet.addButton(
        {
          text: 'View Result',
          handler: () => {
            console.log('View result clicked');
          }
        }
      );
   }
   actionSheet.present();
  }

}
