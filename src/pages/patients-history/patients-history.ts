import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ExaminationListPage } from '../examination-list/examination-list'; 

@Component({
  selector: 'page-patients-history',
  templateUrl: 'patients-history.html',
})
export class PatientsHistoryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatientsHistory');
    
  }

  goToGetHistory() {
     this.navCtrl.push(ExaminationListPage);
    
  }
 
}


