import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { PincodeController } from  'ionic2-pincode-input/dist/pincode'

import { LoginPage } from '../login/login';
import { GetPasswordPage } from '../get-password/get-password';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { ProfilePage } from '../profile/profile';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  code: string;

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public pincodeCtrl: PincodeController) {

  }

  // goToLogin() {
  //   this.navCtrl.push(LoginPage);
  //   // Set root for disabled back button
  //   // this.navCtrl.setRoot(LoginPage, {}, {animate: true, direction: "forward"});
  // }

  goToGetPassword() {
    this.navCtrl.push(GetPasswordPage);
    // Set root for disabled back button
    // this.navCtrl.setRoot(GetPasswordPage, {}, {animate: true, direction: "forward"});
  }

  goToLogin():any{
 
    let pinCode =  this.pincodeCtrl.create({
      title:'Passcode'
    });
 
    pinCode.present();

    pinCode.onWillDismiss((code,status) => {
      if(status == 'done') {
        this.loadingCtrl.create({
          content: "Authenticating...",
          duration: 1000
        }).present();
      }else if (status === 'forgot'){
 
        this.navCtrl.push(ForgotPasswordPage, {}, {animate: true, direction: "forward"});
        // forgot password
      }
    });
 
    pinCode.onDidDismiss( (code,status) => {
      
      if(status === 'done'){
 
        this.code = code;

        // imprement check code with db

        this.navCtrl.setRoot(TabsPage, {}, {animate: true, direction: "forward"});

      }
 
    })
 
  }

}
