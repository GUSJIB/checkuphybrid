import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {

  language: string = 'English';

  constructor(public navCtrl: NavController, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Setting');
  }

  goToPasscode() {
    console.log('go to Passcode');
  }

  goToAbout() {
    let alert = this.alertCtrl.create({
      title: 'About',
      subTitle: 'v0.0.1 beta',
      buttons: ['Dismiss']
    });
    alert.present();
  }

}
