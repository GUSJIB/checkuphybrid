import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

import { DoctorProfilePage } from './doctor-profile';

@Component({
  selector: 'page-doctor',
  templateUrl: 'doctor.html',
})
export class DoctorPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Doctor');
  }

  goToFilter() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Filter by Specialties');

    alert.addInput({
      type: 'checkbox',
      label: 'Anti Aging',
      value: 'value1'
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Cardiology',
      value: 'value2'
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Aviation Medicine',
      value: 'value3'
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Dental',
      value: 'value4'
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'Filter',
      handler: data => {
        console.log('Checkbox data:', data);
      }
    });
    alert.present();
  }

  goToDoctorProfile() {
    this.navCtrl.push(DoctorProfilePage);
  }

}
