import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ExaminationListPage } from '../examination-list/examination-list';

@Component({
  selector: 'page-my-package',
  templateUrl: 'my-package.html',
})
export class MyPackagePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyVoucher');
  }

  goToPackage() {
    this.navCtrl.push(ExaminationListPage);
  }

}
