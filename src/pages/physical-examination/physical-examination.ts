import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-physicalexamination',
  templateUrl: 'physical-examination.html',
})
export class PhysicalExaminationPage {
 tab: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Physicalexamination', this.navParams.get('orderitem'));
  }
 
}
