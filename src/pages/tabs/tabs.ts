import { Component } from '@angular/core';

import { DoctorPage } from '../doctor/doctor';
import { ProfilePage } from '../profile/profile';
import { MyPackagePage } from '../my-package/my-package';
import { MapPage } from '../map/map';
import { PatientsHistoryPage } from '../patients-history/patients-history';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = DoctorPage;
  tab2Root = MyPackagePage;
  tab3Root = ProfilePage;
  tab4Root = PatientsHistoryPage;
  tab5Root = MapPage;

  constructor() {

  }
  
}
