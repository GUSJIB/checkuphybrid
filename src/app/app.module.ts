import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ProfilePage } from '../pages/profile/profile';
import { SettingPage } from '../pages/setting/setting';
import { PatientsPage } from '../pages/patients/patients';
import { LoginPage } from '../pages/login/login';
import { SearchPage } from '../pages/search/search';
import { GetPasswordPage } from '../pages/get-password/get-password';
import { TermsOfServicePage } from '../pages/terms-of-service/terms-of-service';
import { PatientsHistoryPage } from '../pages/patients-history/patients-history';
import { MyPackagePage  } from '../pages/my-package/my-package';
import { ExaminationListPage } from '../pages/examination-list/examination-list';
import { PhysicalExaminationPage } from '../pages/physical-examination/physical-examination';
import { FeaturedPage } from '../pages/featured/featured';
import { MapPage } from '../pages/map/map';
import { VisitSlipPage } from '../pages/visit-slip/visit-slip';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { AppointmentPage } from '../pages/appointment/appointment';
import { DoctorPage } from '../pages/doctor/doctor';
import { DoctorProfilePage } from '../pages/doctor/doctor-profile';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// From Dependency
import { PincodeInputModule } from  'ionic2-pincode-input';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    ProfilePage,
    SettingPage,
    PatientsPage,
    LoginPage,
    SearchPage,
    GetPasswordPage,
    TermsOfServicePage,
    PatientsHistoryPage,
    MyPackagePage,
    ExaminationListPage,
    PhysicalExaminationPage,
    FeaturedPage,
    MapPage,
    VisitSlipPage,
    ForgotPasswordPage,
    AppointmentPage,
    DoctorPage,
    DoctorProfilePage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    PincodeInputModule,
    IonicModule.forRoot(MyApp, {}, {
      links: [
         { component: HomePage, name: 'Home', segment: '' },
         { component: LoginPage, name: 'Login', segment: 'login' },
         { component: GetPasswordPage, name: 'GetPassword', segment: 'get-password' },
         { component: ForgotPasswordPage, name: 'ForgotPassword', segment: 'forgot-password' },
         { component: SearchPage, name: 'Search', segment: 'search' },
         { component: PatientsPage, name: 'Patients', segment: 'patients' },
         { component: ProfilePage, name: 'Profile', segment: 'profile/:id' },
         { component: PatientsHistoryPage, name: 'PatientsHistory', segment: 'patients-history/:id' },
         { component: MapPage, name: 'Map', segment: '' },
         { component: FeaturedPage, name: 'Featured', segment: '' }
      ]
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    ProfilePage,
    SettingPage,
    PatientsPage,
    LoginPage,
    SearchPage,
    GetPasswordPage,
    TermsOfServicePage,
    PatientsHistoryPage,
    MyPackagePage,
    ExaminationListPage,
    PhysicalExaminationPage,
    FeaturedPage,
    MapPage,
    VisitSlipPage,
    ForgotPasswordPage,
    AppointmentPage,
    DoctorPage,
    DoctorProfilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
